#include <iostream>
#include "Nodo.h"

using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

// Clase Arbol
class Arbol{

    private:
        Nodo *raiz = NULL;
        void reestructura_izq(Nodo *&nodo, bool &crece);
        void reestructura_der(Nodo *&nodo, bool &crece);

    public:
        // Constructor default
        Arbol();
        void insertar_numero(Nodo *&nodo, bool &crece, int numero);
        void eliminar_numero(Nodo *&nodo, bool &crece, int numero);
        bool en_arbol(int numero);
        Nodo *&get_raiz();
};
#endif
