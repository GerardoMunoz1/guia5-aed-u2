#include <iostream>
#include <fstream>
#include "Grafo.h"

using namespace std;

// Constructor default
Grafo::Grafo(){}

// Funcion que da inicio a la generación de grafo
void Grafo::generar_grafo(Nodo *nodo){

    ofstream archivo;

    archivo.open("grafo.temp");
    archivo << "digraph G{" << endl;
    archivo << "node [style=filled shape=octagon fillcolor=darkorchid1]" << endl;
    archivo << "null1 [shape=point]" << endl;
    archivo << "null1->" << nodo->numero << " [label=" << nodo->equilibrio << "]" << endl;

    recorrer(nodo, archivo);

    archivo << "}" << endl;
    archivo.close();

    system("dot -Tpng -o grafo.png grafo.temp");
    system("rm grafo.temp");
    system("eog grafo.png");
}

// Funcion para recorrer árbol
void Grafo::recorrer(Nodo *nodo, ofstream &archivo){

    if (nodo != NULL){

        if (nodo->izquierda != NULL){
            archivo << nodo->numero << " -> " << nodo->izquierda->numero << " [arrowshape=vee]"
                    << "[label=" << nodo->izquierda->equilibrio << "]" << endl;
        }

        else{
            archivo << "i" << nodo->numero << " [shape=point]" << endl;
            archivo << nodo->numero << " -> " << "i" << nodo->numero  << endl;
        }

        if (nodo->derecha != NULL){
            archivo << nodo->numero << " -> " << nodo->derecha->numero << " [arrowshape=vee]"
                    << "[label=" << nodo->derecha->equilibrio << "]" << endl;
        }

        else{
            archivo << "d" << nodo->numero << " [shape=point]" << endl;
            archivo << nodo->numero << " -> " << "d" << nodo->numero << endl;
        }

        recorrer(nodo->derecha, archivo);
        recorrer(nodo->izquierda, archivo);
    }
}
