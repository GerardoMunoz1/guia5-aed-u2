#include <iostream>

using namespace std;

#ifndef NODO_H
#define NODO_H

typedef struct _Nodo{
    int numero;
    int equilibrio;
    struct _Nodo *izquierda = NULL;
    struct _Nodo *derecha = NULL;
}Nodo;
#endif
