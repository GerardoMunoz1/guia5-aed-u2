# Guia 5 

Guía 5 - Arboles Balanceados AVL
Gerardo Muñoz
Ingeniería Civil en Bioinformática - Universidad de Talca

## Requisitos previos

Para la completa ejecución del programa es necesario contar con compilador de Gcc y con el paquete Graphviz para la generación de grafos, este último puede ser instalado ejecutando el siguiente comando en una terminal:

>sudo apt-get install graphviz

---

## Compilación y ejecución

Para compilar y ejecutar el programa se deben escribir las siguientes líneas de comando:

>make
>./avl

---


