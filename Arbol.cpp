#include <iostream>
#include "Arbol.h"

using namespace std;

// Constructor default
Arbol::Arbol(){}

// Funcion para la reestructuracion izquierda
void Arbol::reestructura_izq(Nodo *&nodo, bool &crece){

    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL;

    // Si la altura del árbol cambia, se comprueba
    if(crece){

        if(nodo->equilibrio == -1){
            nodo->equilibrio = 0;
        }
        else if(nodo->equilibrio == 0){
            nodo->equilibrio = 1;
            crece = 0;
        }

        else if(nodo->equilibrio == 1){
            nodo1 = nodo->derecha;

            if(nodo1->equilibrio >= 0){
                nodo->derecha = nodo1->izquierda;
                nodo1->izquierda = nodo;

                if(nodo1->equilibrio == 0){
                    nodo->equilibrio = 1;
                    nodo1->equilibrio = -1;
                    crece = 0;
                }
                else if(nodo1->equilibrio == 1){
                    nodo->equilibrio = 0;
                    nodo1->equilibrio = 0;
                }
                nodo = nodo1;
            }

            else{
                nodo2 = nodo1->izquierda;
                nodo->derecha = nodo2->izquierda;
                nodo2->izquierda = nodo;
                nodo1->izquierda = nodo2->derecha;
                nodo2->derecha = nodo1;

                if(nodo2->equilibrio == 1){
                    nodo->equilibrio = -1;
                }
                else{
                    nodo->equilibrio = 0;
                }

                if(nodo2->equilibrio == -1){
                    nodo1->equilibrio = 1;
                }
                else{
                    nodo1->equilibrio = 0;
                }

                nodo = nodo2;
                nodo2->equilibrio = 0;
            }
        }
    }
}

// Funcion para la reestructuracion derecha
void Arbol::reestructura_der(Nodo *&nodo, bool &crece){

    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL;

    // Si la altura del árbol cambia, se comprueba
    if(crece){

        if(nodo->equilibrio == 1){
            nodo->equilibrio = 0;
        }
        else if(nodo->equilibrio == 0){
            nodo->equilibrio = -1;
            crece = 0;
        }

        else if(nodo->equilibrio == -1){
            nodo1 = nodo->izquierda;

            if(nodo1->equilibrio <= 0){
                nodo->izquierda = nodo1->derecha;
                nodo1->derecha = nodo;

                if(nodo1->equilibrio == 0){
                    nodo->equilibrio = -1;
                    nodo1->equilibrio = 1;
                    crece = 0;
                }
                else if(nodo1->equilibrio == -1){
                    nodo->equilibrio = 0;
                    nodo1->equilibrio = 0;
                }
                nodo = nodo1;
            }

            else{
                nodo2 = nodo1->derecha;
                nodo->izquierda = nodo2->derecha;
                nodo2->derecha = nodo;
                nodo1->derecha = nodo2->izquierda;
                nodo2->izquierda = nodo1;

                if(nodo2->equilibrio == -1){
                    nodo->equilibrio = 1;
                }
                else{
                    nodo->equilibrio = 0;
                }

                if(nodo2->equilibrio == 1){
                    nodo1->equilibrio = -1;
                }
                else{
                    nodo1->equilibrio = 0;
                }

                nodo = nodo2;
                nodo2->equilibrio = 0;
            }
        }
    }
}

// Funcion para insertar numero (ID)
void Arbol::insertar_numero(Nodo *&nodo, bool &crece, int numero){


    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL;
    Nodo *otro = NULL;

    // Comprobacion de un nodo vacio
    if(nodo != NULL){

        if(numero < nodo->numero){
            insertar_numero(nodo->izquierda, crece, numero);

            if(crece){

                if(nodo->equilibrio == 1){
                    nodo->equilibrio = 0;
                    crece = 0;
                }
                else if(nodo->equilibrio == 0){
                    nodo->equilibrio = -1;
                }

                else if(nodo->equilibrio == -1){
                    nodo1 = nodo->izquierda;

                    if(nodo1->equilibrio <= 0){
                        nodo->izquierda = nodo1->derecha;
                        nodo1->derecha = nodo;
                        nodo->equilibrio = 0;
                        nodo = nodo1;
                    }

                    else{
                        nodo2 = nodo1->derecha;
                        nodo->izquierda = nodo2->derecha;
                        nodo2->derecha = nodo;
                        nodo1->derecha = nodo2->izquierda;
                        nodo2->izquierda = nodo1;

                        if(nodo2->equilibrio == -1){
                            nodo->equilibrio = 1;
                        }
                        else{
                            nodo->equilibrio = 0;
                        }

                        if(nodo2->equilibrio == 1){
                            nodo1->equilibrio = -1;
                        }
                        else{
                            nodo1->equilibrio = 0;
                        }

                        nodo = nodo2;
                    }
                    nodo->equilibrio = 0;
                    crece = 0;
                }
            }
        }
        else{

            if(numero > nodo->numero){
                insertar_numero(nodo->derecha, crece, numero);

                if(crece){

                    if(nodo->equilibrio == -1){
                        nodo->equilibrio = 0;
                        crece = 0;
                    }
                    else if(nodo->equilibrio == 0){
                        nodo->equilibrio = 1;
                    }

                    else if(nodo->equilibrio == 1){
                        nodo1 = nodo->derecha;

                        if(nodo1->equilibrio >= 0){
                            nodo->derecha = nodo1->izquierda;
                            nodo1->izquierda = nodo;
                            nodo->equilibrio = 0;
                            nodo = nodo1;
                        }

                        else{
                            nodo2 = nodo1->izquierda;
                            nodo->derecha = nodo2->izquierda;
                            nodo2->izquierda = nodo;
                            nodo1->izquierda = nodo2->derecha;
                            nodo2->derecha = nodo1;

                            if(nodo2->equilibrio == 1){
                                nodo->equilibrio = -1;
                            }
                            else{
                                nodo->equilibrio = 0;
                            }

                            if(nodo2->equilibrio == -1){
                                nodo1->equilibrio = 1;
                            }
                            else{
                                nodo1->equilibrio = 0;
                            }
                            nodo = nodo2;
                        }
                        nodo->equilibrio = 0;
                        crece = 0;
                    }
                }
            }
            else{
                cout << "El número ingresado ya se encuentra en el árbol" << endl;
            }
        }
    }
    else{

        nodo = new Nodo();
        nodo->numero = numero;
        nodo->izquierda = NULL;
        nodo->derecha = NULL;
        nodo->equilibrio = 0;
        crece = 1;

        cout << "Número añadido con éxito al árbol!" << endl;
    }
}


// Funcion que elimina algun numero del arbol (ID)
void Arbol::eliminar_numero(Nodo *&nodo, bool &crece, int numero){

    Nodo *otro;
    Nodo *aux1;
    Nodo *aux2;
    bool aux;

    if(nodo != NULL){

        if(numero < nodo->numero){
            eliminar_numero(nodo->izquierda, crece, numero);
            this->reestructura_izq(nodo, crece);
        }

        else if(numero > nodo->numero){
            eliminar_numero(nodo->derecha, crece, numero);
            this->reestructura_der(nodo, crece);
        }

        else{
            otro = nodo;
            crece = 1;

            if(otro->derecha == NULL){
                nodo = otro->izquierda;
            }

            else{
                if(otro->izquierda == NULL){
                    nodo = otro->derecha;
                }

                else{
                    aux1 = nodo->izquierda;
                    aux = 0;

                    while(aux1->derecha != NULL){
                        aux2 = aux1;
                        aux1 = aux1->derecha;
                        aux = 1;
                    }

                    nodo->numero = aux1->numero;
                    otro = aux1;

                    if(aux){
                        aux2->derecha = aux1->izquierda;
                    }

                    else{
                        nodo->izquierda = aux1->izquierda;

                    }

                    free(otro);
                    if(nodo->izquierda != NULL){
                        reestructura_der(nodo->izquierda, crece);
                    }
                }
            }
            cout << "Eliminación exitosa!" << endl;
        }
    }
    // Si se llega a algún nodo nulo, significa que el valor no está en el árbol
    else{
        cout << "La información no se encuentra en el árbol";
    }
}

// Funcion que comprueba si un valor está o no en el arbol
bool Arbol::en_arbol(int numero){

    Nodo *nodo = this->raiz;
    bool encontrado = 0;

    while(nodo != NULL){

        if(nodo->numero == numero){
            encontrado = 1;
            break;
        }
        else if(numero < nodo->numero){
            nodo = nodo->izquierda;
        }
        else if(numero > nodo->numero){
            nodo = nodo->derecha;
        }
    }

    return encontrado;
}
// Funcion que retorna la raiz del arbol
Nodo *&Arbol::get_raiz(){
    return this->raiz;
}
