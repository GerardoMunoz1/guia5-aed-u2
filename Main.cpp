#include <iostream>
#include <stdlib.h>
#include "Arbol.h"
#include "Grafo.h"

using namespace std;

// Funcion con menu principal
void menu(){

    string opcion;
    string numero;
    string numero1;
    Arbol avl;
    Grafo grafo;
    bool crece = 0;

    // Menu infinito, se repetirá hasta que el usuario
    // decida salir.
    while(1){

        cout << "\n\t = MENU PRINCIPAL =\n" << endl;

        cout << "[1] Insertar ID." << endl;
        cout << "[2] Eliminar ID." << endl;
        cout << "[3] Modificar ID." << endl;
        cout << "[4] Generar Grafo." << endl;
        cout << "[0] Salir" << endl << endl;

        cout << "OPCION: ";
        getline(cin, opcion);

        if(opcion == "1"){
            cout << "Ingrese la ID (PDB): ";
            getline(cin, numero);
            avl.insertar_numero(avl.get_raiz(), crece, stoi(numero));
        }

        else if(opcion == "2"){
            cout << "Ingrese número que quiere eliminar del árbol: ";
            getline(cin, numero);
            avl.eliminar_numero(avl.get_raiz(), crece, stoi(numero));
        }

        else if(opcion == "3"){

            cout << "¿Que número desea modificar?: ";
            getline(cin, numero);
            cout << "¿Por cual número quiere reemplazarlo?: ";
            getline(cin, numero1);

            if((!avl.en_arbol(stoi(numero))) && avl.en_arbol(stoi(numero1))){
                cout << "El número que quiere modificar no existe "
                     << "y lo quiere modificar por un número que ya está en el árbol" << endl;
                system("sleep 1");

            }
            else if(avl.en_arbol(stoi(numero1))){
                cout << "El número por el cual quiere cambiar el anterior ya se encuentra en el árbol" << endl;
            }
            else if(!avl.en_arbol(stoi(numero))){
                cout << "El número que quiere modificar no existe" << endl;
            }

            else{
                avl.eliminar_numero(avl.get_raiz(), crece, stoi(numero));
                crece = 0;
                avl.insertar_numero(avl.get_raiz(), crece, stoi(numero1));
                cout << "Modificación realizada con éxito!" << endl;
            }

        }

        else if(opcion == "4"){
            grafo.generar_grafo(avl.get_raiz());
        }

        else if(opcion == "0"){
            cout << "¡Hasta pronto!" << endl;
            exit(0);
        }

        else{
            cout << "La opción ingresada no es válida" << endl;
            system("sleep 1");
            system("clear");
        }

        crece = 0;
        system("sleep 1");
        system("clear");
    }
}

// Funcion Principal
int main(int argc, char **argv){

    if(argc != 1){
        cout << "Error, no se deben ingrear parámetros" << endl;
    }
    else{
        menu();
    }

    return 0;
}
